<?php


function dynamic_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['dynamic_width'] = array(
    '#type' => 'radios',
    '#title' => t('Layout'),
    '#options' => array(
      'fluid' => t('Fluid width'),
      'fixed' => t('Fixed width'),
    ),
    '#default_value' => theme_get_setting('dynamic_width'),
    '#description' => t('Specify whether the content will wrap to a fixed width or will fluidly expand to the width of the browser window.'),
    // Place this above the color scheme options.
    '#weight' => 10,
  );

  $form['dynamic_home_title'] = array(
    '#type' => 'radios',
    '#title' => t('Hide node title on home page'),
    '#options' => array(
      'disable' => t('Disable'),
      'enable' => t('Enable'),
    ),
    '#default_value' => theme_get_setting('dynamic_home_title'),
    '#description' => t('Hide node title on home page'),
    // Place this above the color scheme options.
    '#weight' => 10,
  );


}


