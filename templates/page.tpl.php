<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['title_left']: Dynamic help text, mostly for admin pages.
 * - $page['full_width']: The main content of the current page.
 * - $page['page_top_main']: Items for the first sidebar.
 * - $page['end_first']: Items for the second sidebar.
 * - $page['end_two']: Items for the header region.
 * - $page['end_three']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>


<header>
  <div class="container">
    <div id="header-top" class="row-fluid">
      <div id="logo" class="span3">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
          </a>
        <?php endif; ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><h1
            class="site-name"><?php print $site_name; ?></h1></a>
      </div>
      <div id="mainmenu" class="span9"><?php $mainmenu = menu_tree('main-menu'); print render($mainmenu); ?></div>
    </div>
  </div>
  <?php if ($page['page_top_main']): ?>
    <div class="container paralax">
      <div id="top-block">
        <?php print render($page['page_top_main']); ?>
      </div>
    </div>
  <?php endif; ?>
</header>
<?php if ($page['full_width']): ?>
  <section id="full_width">
    <?php print render($page['full_width']); ?>
  </section>
<?php endif; ?>
<?php if ($page['page_top_first'] || $page['page_top_second'] || $page['page_top_third']): ?>
  <section class="top-section">
    <div class="container">
      <div class="row-fluid">
        <div class="span4">
          <?php print render($page['page_top_first']); ?>
        </div>
        <div class="span4">
          <?php print render($page['page_top_second']); ?>
        </div>
        <div class="span4">
          <?php print render($page['page_top_third']); ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
<?php if ($page['help']): ?>
  <section class="white-section">
    <div class="container">
      <div class="row-fluid">

        <div class="span12">
          <?php print render($page['help']); ?>
        </div>

      </div>
    </div>
  </section>
<?php endif; ?>
<?php if ($title): ?>
  <section class="title-section">
    <div class="container">
      <div class="row-fluid">
        <div class="span8">
          <?php print render($title_prefix); ?>
          <div id="page-title"><h2 class="title"><?php print $title; ?></h2>
            <?php if ($breadcrumb): ?>
            <div id="breadcrumb"><?php print $breadcrumb; ?></div>

          </div><?php endif; ?>


          <?php print render($title_suffix); ?>
        </div>
        <?php if ($page['title_left']): ?>
          <div class="span4">
            <?php print render($page['title_left']); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
<section class="main-section">
  <div class="container">
    <div class="row-fluid">
      <?php if ($page['sidebar_first']): ?>
        <div class="span3">
          <div id="left-sidebar">
            <?php print render($page['sidebar_first']); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="<?php if ($page['sidebar_first'] && $page['sidebar_second']) {
        print 'span5 with-sidebar';
      }
      elseif ($page['sidebar_second']) {
        print 'span8 with-sidebar';
      }
      elseif ($page['sidebar_first']) {
        print 'span9 with-sidebar';
      }
      else {
        print 'span12 without-sidebar';
      }

      ?>
		  
		  ">
        <div id="main-column">


          <div id="main-content">
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs nav-pills">
                <?php print render($tabs); ?>
              </div>
            <?php endif; ?>
            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
          </div>
        </div>
      </div>
      <?php if ($page['sidebar_second']): ?>
        <div class="span4">
          <div id="right-sidebar">
            <?php print render($page['sidebar_second']); ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section> <!-- /.section, /#content -->
<?php if ($page['end_first'] || $page['end_two'] || $page['end_three']): ?>
  <section id="end">
    <div class="container">
      <div class="row-fluid">
        <div class="span4">
          <?php print render($page['end_first']); ?>
          &nbsp;
        </div>
        <div class="span4">
          <?php print render($page['end_two']); ?>
          &nbsp;
        </div>
        <div class="span4">
          <?php print render($page['end_three']); ?>
          &nbsp;
        </div>

      </div>
    </div>
  </section>
<?php endif; ?>
<footer>
  <div class="container">
    <?php if ($page['footer_first']): ?>

      <div class="row-fluid">

        <div class="span3">
          <?php print render($page['footer_first']); ?>
          &nbsp;
        </div>

        <div class="span3">
          <?php print render($page['footer_second']); ?>
          &nbsp;
        </div>

        <div class="span3">
          <?php print render($page['footer_third']); ?>
          &nbsp;
        </div>

        <div class="span3">
          <?php print render($page['footer_four']); ?>
          &nbsp;
        </div>

      </div>
    <?php endif; ?>
    <?php if ($page['footer']): ?>
      <div id="footer-block">
        <?php print render($page['footer']); ?>
      </div>
    <?php endif; ?>
    <div id="footer-copyright" class="row-fluid">
      <div id="author" class="span6">
        <p><?php print l(t('Drupal'), 'http://drupal.org') ?><?php print t(' template made by '); ?><?php print l(t('gastas'), 'http://gastas.net/') ?></p>
      </div>
      <div id="social" class="span6">
        <p class="pull-right"><?php print l(t('Facebook'), 'http://facebook.com/'); ?>
          / <?php print l(t('Twitter'), 'http://twitter.com/'); ?> /<?php print l(t('Google Plus'), 'http://google.com/'); ?></p>
      </div>


    </div>
  </div>
</footer>