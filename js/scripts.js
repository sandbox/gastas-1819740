jQuery(document).ready(function ($) {

    if ($(window).width() > 960 && !($.browser.msie)) {
        var blockheight = $('header').height();

        $('header .paralax').css('height', (blockheight - 200) + 'px');
        $('header .paralax').css('margin-top', '-100px');
        $('header .paralax').css('padding', '100px 0');
        $('header .paralax').css('overflow', 'hidden');

        $('header #top-block .flex-control-nav').css('top', '375px'); //Fix for Flexslider pager

        jQuery(document).scroll(function () {
            s = $(document).scrollTop();
            $('header #top-block').css('margin-top', '' + (s / 4) + 'px');
        })

        jQuery('.portfolio-grid .views-row').hover(
            function () {
                $(this).children('.views-field-title').delay('800').show();
            },
            function () {
                $(this).children('.views-field-title').hide();
            }

        )
    }

    $(window).resize(function () {
        if ($(window).width() < 960) {

            $('header .paralax').removeAttr('style');
            $('header #top-block .flex-control-nav').removeAttr('style');
            $('header #top-block').removeAttr('style');
        }

    })


})(jQuery);