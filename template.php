<?php


/**
 * Override or insert variables into the menu.
 */
function dynamic_preprocess_menu_local_tasks(&$variables) {

  if (!isset($variables['primary']['#pre_render'])) {
    $variables['primary']['#pre_render'] = array();
  }

  array_unshift($variables['primary']['#pre_render'], 'dynamic_del_tabs_css');
}

function dynamic_del_tabs_css($element) {
  $element['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
  $element['#prefix'] .= '<ul class="nav nav-pills">';
  return $element;
}

function dynamic_css_alter(&$css) {
  // Turn off some styles from the system module
  unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);
}


/**
 * Override or insert variables into the form.
 */
function dynamic_form_alter(&$form, &$form_state, $form_id) {
  // Id's of forms that should be ignored
  // Make this configurable?


  if ($form_id == 'search_block_form') {


    $form['search_block_form']['#attributes'] = array('placeholder' => t('Search')); // Add placeholder and class on search textfield
    // $form['search_block_form']['#attributes']['class'][] = 'search-query'; //Add class for twitter bootstrap
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 30; // define size of the textfield
    // $form['#attributes'] = array('class' => 'navbar-search'); //Add class for twitter bootstrap
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#src' => base_path() . path_to_theme() . '/images/searchbutton.png'
    ); //Image button

  }

  if ($form_id == 'commerce_checkout_form_checkout') {

    $form['cart_contents']['#attributes']['class'][] = 'span8'; // Add class on chekout pane
    $form['account']['#attributes']['class'][] = 'span4'; // Add class on chekout pane
    $form['commerce_shipping']['#attributes']['class'][] = 'span4'; // Add class on chekout pane
    $form['customer_profile_shipping']['#attributes']['class'][] = 'span4'; // Add class on chekout pane
    $form['customer_profile_billing']['#attributes']['class'][] = 'span4'; // Add class on chekout pane

    $form['commerce_coupon']['#attributes']['class'][] = 'span8'; // Add class on chekout pane

    $form['buttons']['#attributes']['class'][] = 'span8'; // Add class on chekout pane

  }

}


/**
 * Override or insert variables into the menu.
 */
function dynamic_menu_tree__main_menu($variables) {
  return '<ul class="main-menu nav nav-pills inline clearfix" >' . $variables['tree'] . '</ul>';
}


/**
 * Override or insert variables into the menu.
 */
function dynamic_menu_link__main_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l('<span>' . $element['#title'] . '</span>', $element['#href'], $options = array('html' => TRUE));
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/**
 * Override or insert variables into the page template.
 */
function dynamic_process_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  //Adding Twitter Bootstrap CSS

  drupal_add_css(path_to_theme() . '/css/bootstrap.min.css', 'theme', 'all', TRUE);

  if (theme_get_setting('dynamic_width') == 'fluid') { //Adding Twitter Bootstrap Responsive CSS for fluid mode
    drupal_add_css(path_to_theme() . '/css/bootstrap-responsive.min.css', 'theme', 'all', TRUE);

  }

  $vars['styles'] = drupal_get_css();

  if (theme_get_setting('dynamic_home_title') == 'enable' && drupal_is_front_page()) {
    $vars['title'] = NULL; // Hide the node title before the node is getting rendered.


  }


}

/**
 * Override or insert variables into the page template.
 */
function dynamic_preprocess_node(&$vars) {

  $vars['submitted'] = t('<span class="author vcard"><i class="icon-user"></i>!username</span><span class="date"><i class="icon-time"></i>!datetime</a></span>',
    array(
      '!username' => $vars['name'],
      '!datetime' => $vars['date'],
    ));

}

/**
 * Override or insert variables into the comment template.
 */
function dynamic_preprocess_comment(&$vars) {
  $vars['title'] = $vars['author'];
  $vars['permalink'] = NULL;
  $vars['submitted'] = t('<span class="date">!datetime</a></span>',
    array(
      '!datetime' => $vars['created'],
    ));
}

/**
 * Override or insert variables into the comment template.
 */
function dynamic_process_html(&$vars) {
  // Hook into color.module

  if (module_exists('color')) {
    _color_html_alter($vars);
  }


}

function dynamic_button(&$variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  $element['#attributes']['class'][] = 'btn btn-primary';

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}


