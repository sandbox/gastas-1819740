<?php

$info = array(

  // Available colors and color labels used in theme.
  'fields' => array(
    'base' => t('Base color'),
    'link' => t('Link color'),
    'header' => t('Header top'),
    'header_link' => t('Header link'),
    'footer' => t('Footer'),
    'text' => t('Text color'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Green (Default)'),
      'colors' => array(
        'base' => '#009933',
        'link' => '#009933',
        'header' => '#009933',
        'header_link' => '#fff',
        'text' => '#494949',
      ),
    ),
    'blue' => array(
      'title' => t('Blue'),
      'colors' => array(
        'base' => '#235685',
        'link' => '#0088cc',
        'header' => '#235685',
        'header_link' => '#fff',
        'footer' => '#000',
      ),
    ),
//    'green' => array(
//      'title' => t('Orange'),
//      'colors' => array(
//        'base' => '#DF8A2A',
//        'link' => '#FE8506',
//        'header' => '#DF8A2A',
//        'header_link' => '#fff',
//        'footer' => '#000',
//      ),
//    ),
  ),


  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'style.css',
  ),
  
  
  'gradients' => array(    ),


  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 760, 568),
    'link' => array(107, 533, 41, 23),
    'footer' => array(107, 533, 41, 23),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/body.png'                      => array(0, 37, 1, 280),
    'images/bg-bar.png'                    => array(202, 530, 76, 14),
    'images/bg-bar-white.png'              => array(202, 506, 76, 14),
    'images/bg-tab.png'                    => array(107, 533, 41, 23),
    'images/bg-navigation.png'             => array(0, 0, 7, 37),
    'images/bg-content-left.png'           => array(40, 117, 50, 352),
    'images/bg-content-right.png'          => array(510, 117, 50, 352),
    'images/bg-content.png'                => array(299, 117, 7, 200),
    'images/bg-navigation-item.png'        => array(32, 37, 17, 12),
    'images/bg-navigation-item-hover.png'  => array(54, 37, 17, 12),
    'images/gradient-inner.png'            => array(646, 307, 112, 42),

    'logo.png'                             => array(622, 51, 64, 73),
    'screenshot.png'                       => array(0, 37, 400, 240),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);